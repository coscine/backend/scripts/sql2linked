﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SQL2Linked.Implementations;
using SQL2Linked.Models.ConfigurationModels;
using Winton.Extensions.Configuration.Consul;

namespace SQL2Linked;

public class Program
{
    private static IServiceProvider _serviceProvider = null!;

    private static async Task Main(string[] args)
    {
        InitializeServices();

        var sql2LinkedConfiguration = _serviceProvider.GetRequiredService<IOptionsMonitor<SQL2LinkedConfiguration>>().CurrentValue;
        if (!sql2LinkedConfiguration.IsEnabled)
        {
            Console.WriteLine("SQL 2 Linked Data migration is disabled. To enable it, set the \"IsEnabled\" property to \"true\" in the configuration.");
            return;
        }

        var dummyMode = !(args.Length > 0 && args[0] == "--noDryRun");
        if (dummyMode)
        {
            Console.WriteLine("\n DUMMY MODE \n");
            Console.WriteLine(" To exit dummy mode, execute with the \"--noDryRun\" argument");
        }

        Console.WriteLine("\nBegin SQL 2 Linked Data migration");

        var roleStructuralData = _serviceProvider.GetRequiredService<RoleStructuralData>();
        await roleStructuralData.Migrate(dummyMode);

        var userStructuralData = _serviceProvider.GetRequiredService<UserStructuralData>();
        await userStructuralData.Migrate(dummyMode);

        var resourceTypeStructuralData = _serviceProvider.GetRequiredService<ResourceTypeStructuralData>();
        await resourceTypeStructuralData.Migrate(dummyMode);

        var projectStructuralData = _serviceProvider.GetRequiredService<ProjectStructuralData>();
        await projectStructuralData.Migrate(dummyMode);

        var resourceStructuralData = _serviceProvider.GetRequiredService<ResourceStructuralData>();
        await resourceStructuralData.Migrate(dummyMode);

        Console.WriteLine("\n Finished.");
    }

    private static void InitializeServices()
    {
        // Create a new instance of ConfigurationBuilder
        var configBuilder = new ConfigurationBuilder();

        // Define the Consul URL
        var consulUrl = Environment.GetEnvironmentVariable("CONSUL_URL") ?? "http://localhost:8500";

        // Remove the default sources
        configBuilder.Sources.Clear();

        // Add Consul as a configuration source
        var configuration = configBuilder
            .AddConsul(
                "coscine/Coscine.Infrastructure/SQL2Linked/appsettings",
                options =>
                {
                    options.ConsulConfigurationOptions =
                        cco => cco.Address = new Uri(consulUrl);
                    options.Optional = true;
                    options.ReloadOnChange = true;
                    options.PollWaitTime = TimeSpan.FromSeconds(5);
                    options.OnLoadException = exceptionContext => exceptionContext.Ignore = true;
                }
            )
            .AddEnvironmentVariables()
            .Build();

        var services = new ServiceCollection()
            .AddSingleton<IConfiguration>(configuration);

        // Add the configuration to the service collection
        services.Configure<SQL2LinkedConfiguration>(settings =>
        {
            configuration.GetSection(SQL2LinkedConfiguration.Section).Bind(settings);
        });

        // Add the services to the service collection
        services.AddTransient<ProjectStructuralData>();
        services.AddTransient<ResourceStructuralData>();
        services.AddTransient<ResourceTypeStructuralData>();
        services.AddTransient<RoleStructuralData>();
        services.AddTransient<UserStructuralData>();

        _serviceProvider = services.BuildServiceProvider();
    }
}