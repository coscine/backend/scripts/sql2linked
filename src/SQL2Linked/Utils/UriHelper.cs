﻿namespace SQL2Linked.Utils;

/// <summary>
/// 
/// </summary>
public static class UriHelper
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="baseUri"></param>
    /// <param name="relativePath"></param>
    /// <returns></returns>
    public static Uri? TryCombinePath(Uri baseUri, string relativePath)
    {
        Uri.TryCreate(baseUri, relativePath + "/", out Uri? result);
        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="baseUri"></param>
    /// <param name="relativePath"></param>
    /// <returns></returns>
    public static Uri? TryCombinePath(Uri baseUri, Guid relativePath)
    {
        Uri.TryCreate(baseUri, relativePath.ToString() + "/", out Uri? result);
        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="baseUri"></param>
    /// <param name="relativePath"></param>
    /// <returns></returns>
    public static Uri? TryCombineUri(Uri baseUri, string relativePath)
    {
        Uri.TryCreate(baseUri, relativePath, out Uri? result);
        return result;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="baseUri"></param>
    /// <param name="relativePath"></param>
    /// <returns></returns>
    public static Uri? TryCombineUri(Uri baseUri, Guid relativePath)
    {
        Uri.TryCreate(baseUri, relativePath.ToString(), out Uri? result);
        return result;
    }
}
