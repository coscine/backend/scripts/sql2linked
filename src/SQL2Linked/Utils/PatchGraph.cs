﻿using VDS.RDF;

namespace SQL2Linked.Utils;

/// <summary>
/// Represents a specialized RDF graph for small-scale updates.
/// This class extends the standard Graph class with additional
/// functionalities for tracking changes (assertions and retractions).
/// </summary>
/// <remarks><i>TODO: Consider extending <see cref="ITransactionalGraph"/> to allow for rollback and commit operations.</i></remarks>
public class PatchGraph : Graph
{
    public List<Triple> AssertList { get; set; } = new();

    public List<Triple> RetractList { get; set; } = new();

    /// <summary>
    /// Initializes a new instance of the <see cref="PatchGraph"/> class.
    /// </summary>
    public PatchGraph() : base()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PatchGraph"/> class.
    /// </summary>
    public PatchGraph(Uri graphUri) : base(graphUri)
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="IPatchGraph"/> class with the specified graph URI.
    /// </summary>
    /// <param name="graphUri">The base URI for the graph.</param>
    /// <returns>A new empty instance of <see cref="IPatchGraph"/> with the provided base URI.</returns>
    public static PatchGraph Empty(Uri graphUri)
    {
        return new PatchGraph(graphUri)
        {
            BaseUri = graphUri
        };
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="IPatchGraph"/> class with the specified graph URI.
    /// </summary>
    /// <param name="graphUri">The base URI for the graph as a string.</param>
    /// <returns>A new empty instance of <see cref="IPatchGraph"/> with the provided base URI.</returns>
    public static PatchGraph Empty(string graphUri)
    {
        return Empty(new Uri(graphUri));
    }

    public override bool Assert(Triple t)
    {
        AssertList.Add(t);
        return base.Assert(t);
    }

    public override bool Assert(IEnumerable<Triple> triples)
    {
        AssertList.AddRange(triples);
        return base.Assert(triples);
    }

    public override bool Retract(Triple t)
    {
        RetractList.Add(t);
        return base.Retract(t);
    }

    public override bool Retract(IEnumerable<Triple> triples)
    {
        RetractList.AddRange(triples);
        return base.Retract(triples);
    }
}