﻿// Ignore Spelling: Foaf Dcat Dcterms Ebocore Fdp Ldp Xsd

/// ------------------
/// TAKEN FROM THE API
/// ------------------

namespace SQL2Linked.Utils;

/// <summary>
/// Contains URIs relevant to RDF (Resource Description Framework) and related schemas/ontologies.
/// </summary>
public static class RdfUris
{
    // ACL
    public static readonly Uri AclPrefix = new("http://www.w3.org/ns/auth/acl#");

    public static readonly Uri AclAccessTo = new("http://www.w3.org/ns/auth/acl#accessTo");
    public static readonly Uri AclAgentGroup = new("http://www.w3.org/ns/auth/acl#agentGroup");
    public static readonly Uri AclDefault = new("http://www.w3.org/ns/auth/acl#default");
    public static readonly Uri AclMode = new("http://www.w3.org/ns/auth/acl#mode");

    public static readonly Uri AclAuthorizationClass = new("http://www.w3.org/ns/auth/acl#Authorization");
    public static readonly Uri AclReadClass = new("http://www.w3.org/ns/auth/acl#Read");
    public static readonly Uri AclWriteClass = new("http://www.w3.org/ns/auth/acl#Write");

    // Coscine
    public static readonly Uri CoscinePrefix = new("https://purl.org/coscine/");

    public static readonly Uri CoscineApplicationProfile = new("https://purl.org/coscine/ap/");
    public static readonly Uri CoscineFixedValue = new("https://purl.org/coscine/fixedValue");
    public static readonly Uri CoscineMetadataExtractionVersion = new("https://purl.org/coscine/terms/metatadataextraction#version");
    public static readonly Uri CoscineProjectsEntity = new("https://purl.org/coscine/projects");
    public static readonly Uri CoscineProjects = new("https://purl.org/coscine/projects/");
    public static readonly Uri CoscineResourceTypes = new("https://purl.org/coscine/resourcetypes/");
    public static readonly Uri CoscineResourcesEntity = new("https://purl.org/coscine/resources");
    public static readonly Uri CoscineResources = new("https://purl.org/coscine/resources/");
    public static readonly Uri CoscineRoles = new("https://purl.org/coscine/roles/");
    public static readonly Uri CoscineUsers = new("https://purl.org/coscine/users/");

    [Obsolete($"Beware the side-effects before changing! Use {nameof(CoscineUsers)} instead.")]
    public static readonly Uri CoscineUserLegacy = new("https://coscine.rwth-aachen.de/u/");

    // Coscine Terms
    public static readonly Uri CoscineTermsPrefix = new("https://purl.org/coscine/terms/");

    public static readonly Uri CoscineTermsLinkedBody = new("https://purl.org/coscine/terms/linked#body");
    public static readonly Uri CoscineTermsMetadataTrackerAgent = new("https://purl.org/coscine/terms/metadatatracker#Agent");
    public static readonly Uri CoscineTermsProject = new("https://purl.org/coscine/terms/project#");
    public static readonly Uri CoscineTermsProjectDeleted = new("https://purl.org/coscine/terms/project#deleted");
    public static readonly Uri CoscineTermsProjectSlug = new("https://purl.org/coscine/terms/project#slug");
    public static readonly Uri CoscineTermsProjectVisibility = new("https://purl.org/coscine/terms/project#visibility");
    public static readonly Uri CoscineTermsResource = new("https://purl.org/coscine/terms/resource#");
    public static readonly Uri CoscineTermsResourceArchived = new("https://purl.org/coscine/terms/resource#archived");
    public static readonly Uri CoscineTermsResourceDeleted = new("https://purl.org/coscine/terms/resource#deleted");
    public static readonly Uri CoscineTermsResourceFixedValues = new("https://purl.org/coscine/terms/resource#fixedValues");
    public static readonly Uri CoscineTermsResourceVisibility = new("https://purl.org/coscine/terms/resource#visibility");
    public static readonly Uri CoscineTermsTypes = new("https://purl.org/coscine/terms/types#");
    public static readonly Uri CoscineTermsUserAgent = new("https://purl.org/coscine/terms/user#Agent");
    public static readonly Uri CoscineTermsVisibility = new("https://purl.org/coscine/terms/visibility#");
    public static readonly Uri CoscineTermsVisibilityPublic = new("https://purl.org/coscine/terms/visibility#public");
    public static readonly Uri CoscineTermsVisibilityProjectMember = new("https://purl.org/coscine/terms/visibility#projectMember");
    public static readonly Uri CoscineTermsRole = new("https://purl.org/coscine/terms/role#");

    // CSMD
    public static readonly Uri CsmdPrefix = new("http://www.purl.org/net/CSMD/4.0#");

    // DCAT
    public static readonly Uri DcatPrefix = new("http://www.w3.org/ns/dcat#");

    public static readonly Uri DcatCatalog = new("http://www.w3.org/ns/dcat#catalog");
    public static readonly Uri DcatDataset = new("http://www.w3.org/ns/dcat#dataset");
    public static readonly Uri DcatDistribution = new("http://www.w3.org/ns/dcat#distribution");
    public static readonly Uri DcatService = new("http://www.w3.org/ns/dcat#service");

    public static readonly Uri DcatCatalogClass = new("http://www.w3.org/ns/dcat#Catalog");
    public static readonly Uri DcatDataServiceClass = new("http://www.w3.org/ns/dcat#DataService");
    public static readonly Uri DcatDatasetClass = new("http://www.w3.org/ns/dcat#Dataset");

    // DCMI Type
    public static readonly Uri DcmiTypePrefix = new("http://purl.org/dc/dcmitype/");

    // DC
    public static readonly Uri DcPrefix = new("http://purl.org/dc/elements/1.1/");

    // DC Terms
    public static readonly Uri DcTermsPrefix = new("http://purl.org/dc/terms/");

    public static readonly Uri DcTermsAlternative = new("http://purl.org/dc/terms/alternative");
    public static readonly Uri DcTermsConformsTo = new("http://purl.org/dc/terms/conformsTo");
    public static readonly Uri DcTermsCreator = new("http://purl.org/dc/terms/creator");
    public static readonly Uri DcTermsCreated = new("http://purl.org/dc/terms/created");
    public static readonly Uri DcTermsDescription = new("http://purl.org/dc/terms/description");
    public static readonly Uri DcTermsEndDate = new("http://purl.org/dc/terms/endDate");
    public static readonly Uri DcTermsIdentifier = new("http://purl.org/dc/terms/identifier");
    public static readonly Uri DcTermsIsPartOf = new("http://purl.org/dc/terms/isPartOf");
    public static readonly Uri DcTermsLicense = new("http://purl.org/dc/terms/license");
    public static readonly Uri DcTermsModified = new("http://purl.org/dc/terms/modified");
    public static readonly Uri DcTermsRights = new("http://purl.org/dc/terms/rights");
    public static readonly Uri DcTermsRightsHolder = new("http://purl.org/dc/terms/rightsHolder");
    public static readonly Uri DcTermsStartDate = new("http://purl.org/dc/terms/startDate");
    public static readonly Uri DcTermsSubject = new("http://purl.org/dc/terms/subject");
    public static readonly Uri DcTermsTitle = new("http://purl.org/dc/terms/title");

    // EBUCORE
    public static readonly Uri EbocorePrefix = new("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#");

    public static readonly Uri EbocoreHashFunction = new("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashFunction");
    public static readonly Uri EbocoreHashType = new("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashType");
    public static readonly Uri EbocoreHashValue = new("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashValue");

    // FDP
    public static readonly Uri FdpPrefix = new("http://purl.org/fdp/fdp-o#");

    public static readonly Uri FdpHasMetadata = new("http://purl.org/fdp/fdp-o#hasMetadata");
    public static readonly Uri FdpIsMetadataOf = new("http://purl.org/fdp/fdp-o#isMetadataOf");

    public static readonly Uri FdpMetadataServiceClass = new("http://purl.org/fdp/fdp-o#MetadataService");

    // FOAF
    public static readonly Uri FoafPrefix = new("http://xmlns.com/foaf/0.1/");

    public static readonly Uri FoafHomepage = new("http://xmlns.com/foaf/0.1/homepage");
    public static readonly Uri FoafName = new("http://xmlns.com/foaf/0.1/name");

    public static readonly Uri FoafPersonClass = new("http://xmlns.com/foaf/0.1/Person");

    // Handle
    public static readonly Uri HandlePrefix = new("https://hdl.handle.net/");

    // LDP
    public static readonly Uri LdpPrefix = new("http://www.w3.org/ns/ldp#");

    public static readonly Uri LdpDescribedBy = new("http://www.w3.org/ns/ldp#describedBy");

    public static readonly Uri LdpBasicContainerClass = new("http://www.w3.org/ns/ldp#BasicContainer");
    public static readonly Uri LdpNonRDFSourceClass = new("http://www.w3.org/ns/ldp#NonRDFSource");
    public static readonly Uri LdpRDFSourceClass = new("http://www.w3.org/ns/ldp#RDFSource");

    // ORG
    public static readonly Uri OrgPrefix = new("http://www.w3.org/ns/org#");

    public static readonly Uri OrgMember = new("http://www.w3.org/ns/org#member");
    public static readonly Uri OrgOrganization = new("http://www.w3.org/ns/org#organization");
    public static readonly Uri OrgRole = new("http://www.w3.org/ns/org#role");

    public static readonly Uri OrgMembershipClass = new("http://www.w3.org/ns/org#Membership");
    public static readonly Uri OrgOrganizationalCollaborationClass = new("http://www.w3.org/ns/org#OrganizationalCollaboration");
    public static readonly Uri OrgRoleClass = new("http://www.w3.org/ns/org#Role");

    // OWL
    public static readonly Uri OwlPrefix = new("http://www.w3.org/2002/07/owl#");

    public static readonly Uri OwlImports = new("http://www.w3.org/2002/07/owl#imports");

    // PIM
    public static readonly Uri PimPrefix = new("http://www.w3.org/ns/pim/space#");

    public static readonly Uri PimStorageClass = new("http://www.w3.org/ns/pim/space#Storage");

    // PROV
    public static readonly Uri ProvPrefix = new("http://www.w3.org/ns/prov#");

    public static readonly Uri ProvEntityClass = new("http://www.w3.org/ns/prov#Entity");

    public static readonly Uri ProvGeneratedAtTime = new("http://www.w3.org/ns/prov#generatedAtTime");
    public static readonly Uri ProvWasInvalidatedBy = new("http://www.w3.org/ns/prov#wasInvalidatedBy");
    public static readonly Uri ProvWasRevisionOf = new("http://www.w3.org/ns/prov#wasRevisionOf");

    // RDF
    public static readonly Uri RdfUrlPrefix = new("http://www.w3.org/1999/02/22-rdf-syntax-ns#");

    public static readonly Uri A = new("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");

    // RDFS
    public static readonly Uri RdfsUrlPrefix = new("http://www.w3.org/2000/01/rdf-schema#");

    public static readonly Uri SubClassOf = new("http://www.w3.org/2000/01/rdf-schema#subClassOf");

    // ROR IDs
    /// <summary>
    /// Represents the RWTH Aachen's ROR (Research Organization Registry) ID.
    /// </summary>
    public static readonly Uri RwthRorId = new("https://ror.org/04xfq0f34");

    // SCHEMA
    public static readonly Uri SchemaPrefix = new("http://schema.org/");

    public static readonly Uri SchemaFunding = new("http://schema.org/funding");

    // SHACL
    public static readonly Uri ShaclPrefix = new("http://www.w3.org/ns/shacl#");

    public static readonly Uri ShaclDatatype = new("http://www.w3.org/ns/shacl#datatype");
    public static readonly Uri ShaclPath = new("http://www.w3.org/ns/shacl#path");
    public static readonly Uri ShaclProperty = new("http://www.w3.org/ns/shacl#property");
    public static readonly Uri ShaclTargetClass = new("http://www.w3.org/ns/shacl#targetClass");
    public static readonly Uri Class = new("http://www.w3.org/ns/shacl#class");

    // Trellis
    public static readonly Uri TrellisPrefix = new("http://www.trellisldp.org/ns/trellis#");

    public static readonly Uri TrellisGraph = new("http://www.trellisldp.org/ns/trellis#PreferServerManaged");

    // Vcard
    public static readonly Uri VcardPrefix = new("http://www.w3.org/2006/vcard/ns#");

    public static readonly Uri VcardHasMember = new("http://www.w3.org/2006/vcard/ns#hasMember");

    public static readonly Uri VcardGroupClass = new("http://www.w3.org/2006/vcard/ns#Group");

    // XSD
    public static readonly Uri XsdPrefix = new("http://www.w3.org/2001/XMLSchema#");

    public static readonly Uri XsdDateTime = new("http://www.w3.org/2001/XMLSchema#dateTime");
    public static readonly Uri XsdBoolean = new("http://www.w3.org/2001/XMLSchema#boolean");
    public static readonly Uri XsdHexBinary = new("http://www.w3.org/2001/XMLSchema#hexBinary");
}