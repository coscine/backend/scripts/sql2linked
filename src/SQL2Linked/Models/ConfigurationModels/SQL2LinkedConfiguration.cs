﻿namespace SQL2Linked.Models.ConfigurationModels;

/// <summary>
/// Represents the configuration settings used in the application.
/// </summary>
public class SQL2LinkedConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "SQL2LinkedConfiguration";

    /// <summary>
    /// Value indicating whether SQL2Linked is enabled.
    /// </summary>
    public bool IsEnabled { get; init; }
}