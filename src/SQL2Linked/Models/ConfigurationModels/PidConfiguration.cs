﻿namespace SQL2Linked.Models.ConfigurationModels;

/// <summary>
/// Represents the configuration settings for the PID ePIC API used in the application.
/// </summary>
public class PidConfiguration
{
    /// <summary>
    /// The section name in the configuration file.
    /// </summary>
    public static readonly string Section = "PidConfiguration";

    /// <summary>
    /// Prefix for the PID ePIC API.
    /// </summary>
    public string Prefix { get; init; } = null!;
}
