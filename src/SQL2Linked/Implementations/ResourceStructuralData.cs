using Coscine.ApiClient;
using Coscine.ApiClient.Core.Model;
using Newtonsoft.Json;
using SQL2Linked.Utils;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace SQL2Linked.Implementations;

/// <summary>
/// Class responsible for converting resource data into linked data graphs.
/// It retrieves resource information from the API and then transforms this data into a series of RDF graphs,
/// making use of predefined URIs and RDF constructs.
/// </summary>
public class ResourceStructuralData : StructuralData<ResourceAdminDto>
{
    public override IAsyncEnumerable<ResourceAdminDto> GetAll() =>
        PaginationHelper.GetAllAsync<ResourceAdminDtoPagedResponse, ResourceAdminDto>(
            (currentPage) => _adminApi.GetAllResourcesAsync(includeDeleted: true, pageNumber: currentPage, pageSize: 50));

    public override async IAsyncEnumerable<IGraph> ConvertToLinkedDataAsync(IAsyncEnumerable<ResourceAdminDto> entries)
    {
        var coscineHandlePrefix = UriHelper.TryCombinePath(RdfUris.HandlePrefix, _pidConfiguration.Prefix)
            ?? throw new Exception("Could not combine handle prefix with PID prefix");

        var trellisGraph = PatchGraph.Empty(RdfUris.TrellisGraph);

        await foreach (var entry in entries)
        {
            var resourceGraphName = UriHelper.TryCombineUri(RdfUris.CoscineResources, entry.Id)
                ?? throw new Exception("Could not combine resources prefix with resource ID");
            var resourceHandleName = UriHelper.TryCombineUri(coscineHandlePrefix, entry.Id);

            var graph = new Graph
            {
                BaseUri = resourceGraphName
            };

            AssertToGraphUriNode(graph, resourceGraphName, RdfUris.A, RdfUris.DcatCatalogClass);
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.A} {RdfUris.DcatCatalogClass}'. ");

            AssertToGraphUriNode(graph, resourceGraphName, RdfUris.A, RdfUris.PimStorageClass);
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.A} {RdfUris.PimStorageClass}'. ");

            AssertToGraphUriNode(graph, resourceGraphName, RdfUris.DcatService, UriHelper.TryCombineUri(RdfUris.CoscineResourceTypes, entry.Type.Id));
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcatService} {UriHelper.TryCombineUri(RdfUris.CoscineResourceTypes, entry.Type.Id)}'. ");

            AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.DcTermsTitle, entry.Name);
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsTitle} {entry.Name}'. ");

            AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.DcTermsAlternative, entry.DisplayName);
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsAlternative} {entry.DisplayName}'. ");

            if (entry.Visibility.DisplayName.Contains("Public"))
            {
                AssertToGraphUriNode(graph, resourceGraphName, RdfUris.CoscineTermsResourceVisibility, RdfUris.CoscineTermsVisibilityPublic);
                Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.CoscineTermsResourceVisibility} {RdfUris.CoscineTermsVisibilityPublic}'. ");
            }
            else if (entry.Visibility.DisplayName.Contains("Project Members"))
            {
                AssertToGraphUriNode(graph, resourceGraphName, RdfUris.CoscineTermsResourceVisibility, RdfUris.CoscineTermsVisibilityProjectMember);
                Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.CoscineTermsResourceVisibility} {RdfUris.CoscineTermsVisibilityProjectMember}'. ");
            }

            if (entry.License is not null)
            {
                AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.DcTermsLicense, entry.License.DisplayName);
                Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsLicense} {entry.License.DisplayName}'. ");
            }

            if (entry.Keywords.Count > 0)
            {
                foreach (var keyword in entry.Keywords)
                {
                    AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.DcTermsSubject, keyword);
                    Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsSubject} {keyword}'. ");
                }
            }

            AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.DcTermsRights, entry.UsageRights);
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsRights} {entry.UsageRights}'. ");

            AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.DcTermsDescription, entry.Description);
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsDescription} {entry.Description}'. ");

            // Skipping broken resources
            if (string.IsNullOrWhiteSpace(entry.ApplicationProfile.Uri))
            {
                continue;
            }

            AssertToGraphUriNode(graph, resourceGraphName, RdfUris.DcTermsConformsTo, new Uri(entry.ApplicationProfile.Uri));
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsConformsTo} {entry.ApplicationProfile}'. ");

            AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.CoscineTermsResourceFixedValues, JsonConvert.SerializeObject(entry.FixedValues));
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.CoscineTermsResourceFixedValues} {JsonConvert.SerializeObject(entry.FixedValues)}'. ");

            if (entry.Creator is not null)
            {
                AssertToGraphUriNode(graph, resourceGraphName, RdfUris.DcTermsCreator, UriHelper.TryCombineUri(RdfUris.CoscineUsers, entry.Creator.Id));
                Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsCreator} {UriHelper.TryCombineUri(RdfUris.CoscineUsers, entry.Creator.Id)}'. ");
            }

            AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.CoscineTermsResourceArchived, entry.Archived.ToString().ToLower(), new Uri("http://www.w3.org/2001/XMLSchema#boolean"));
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.CoscineTermsResourceArchived} {entry.Archived}'. ");

            AssertToGraphUriNode(graph, resourceGraphName, RdfUris.FoafHomepage, resourceHandleName);
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.FoafHomepage} {resourceGraphName}'. ");

            AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.CoscineTermsResourceDeleted, entry.Deleted.ToString().ToLower(), new Uri("http://www.w3.org/2001/XMLSchema#boolean"));
            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.CoscineTermsResourceDeleted} {entry.Deleted}'. ");

            // Reinstate the catalog assignments
            var oldResourceGraphResponse = await _adminApi.GetMetadataGraphAsync(
                resourceGraphName.AbsoluteUri,
                RdfFormat.TextTurtle
            );
            if (oldResourceGraphResponse is not null)
            {
                var oldResourceGraph = new Graph();
                var ttlparser = new TurtleParser();
                ttlparser.Load(oldResourceGraph, new StringReader(oldResourceGraphResponse.Data.Content));

                foreach (var result in oldResourceGraph.GetTriplesWithPredicate(RdfUris.DcatCatalog))
                {
                    if (result.Object.NodeType == NodeType.Uri)
                    { 
                        var catalogedUri = result.Object as IUriNode;
                        if (catalogedUri is not null)
                        {
                            AssertToGraphUriNode(graph, resourceGraphName, RdfUris.DcatCatalog, catalogedUri.Uri);
                            Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcatCatalog} {catalogedUri.Uri}'. ");
                        }
                    }
                }
            }

            foreach (var projectResource in entry.ProjectResources)
            {
                if (entry.Id == projectResource.ResourceId)
                {
                    var blankNode = graph.CreateBlankNode();

                    AssertToGraphBlankAndUriNode(graph, blankNode, RdfUris.A, RdfUris.AclAuthorizationClass);
                    Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{blankNode} {RdfUris.A} {RdfUris.AclAuthorizationClass}'. ");

                    AssertToGraphBlankAndUriNode(graph, blankNode, RdfUris.AclAgentGroup, UriHelper.TryCombineUri(RdfUris.CoscineProjects, projectResource.ProjectId));
                    Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{blankNode} {RdfUris.AclAgentGroup} {UriHelper.TryCombineUri(RdfUris.CoscineProjects, projectResource.ProjectId)}'. ");

                    AssertToGraphBlankAndUriNode(graph, blankNode, RdfUris.AclAccessTo, resourceGraphName);
                    Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{blankNode} {RdfUris.AclAccessTo} {resourceGraphName}'. ");

                    AssertToGraphBlankAndUriNode(graph, blankNode, RdfUris.AclDefault, resourceGraphName);
                    Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{blankNode} {RdfUris.AclDefault} {resourceGraphName}'. ");

                    AssertToGraphBlankAndUriNode(graph, blankNode, RdfUris.AclMode, RdfUris.AclReadClass);
                    Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{blankNode} {RdfUris.AclMode} {RdfUris.AclReadClass}'. ");

                    AssertToGraphBlankAndUriNode(graph, blankNode, RdfUris.AclMode, RdfUris.AclWriteClass);
                    Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{blankNode} {RdfUris.AclMode} {RdfUris.AclWriteClass}'. ");
                }
            }

            if (entry.DateCreated is not null && entry.DateCreated.HasValue)
            {
                AssertToGraphLiteralNode(graph, resourceGraphName, RdfUris.DcTermsCreated, entry.DateCreated.Value.ToString(), new Uri("http://www.w3.org/2001/XMLSchema#dateTime"));
                Console.WriteLine($"For resource '{entry.DisplayName}' will migrate triple '{resourceGraphName} {RdfUris.DcTermsCreated} {entry.DateCreated}'. ");
            }

            // Add data to the trellis graph
            AssertToGraphUriNode(trellisGraph,
                    resourceGraphName,
                    RdfUris.A,
                    RdfUris.LdpBasicContainerClass);
            AddModifiedDate(trellisGraph, resourceGraphName);

            AssertToGraphUriNode(trellisGraph,
                    resourceGraphName,
                    RdfUris.DcTermsIsPartOf,
                    RdfUris.CoscineResourcesEntity);
            AssertToGraphUriNode(trellisGraph,
                    RdfUris.CoscineResourcesEntity,
                    RdfUris.A,
                    RdfUris.LdpBasicContainerClass);
            AddModifiedDate(trellisGraph, RdfUris.CoscineResourcesEntity);

            AssertToGraphUriNode(trellisGraph,
                    RdfUris.CoscineResourcesEntity,
                    RdfUris.DcTermsIsPartOf,
                    RdfUris.CoscinePrefix);
            AssertToGraphUriNode(trellisGraph,
                    RdfUris.CoscinePrefix,
                    RdfUris.A,
                    RdfUris.LdpBasicContainerClass);
            AddModifiedDate(trellisGraph, RdfUris.CoscinePrefix);

            var resourceACLGraphName = UriHelper.TryCombineUri(resourceGraphName, "?ext=acl");
            var aclGraph = new Graph()
            {
                BaseUri = resourceACLGraphName
            };

            var aclSubjects = graph.GetTriplesWithObject(RdfUris.AclAuthorizationClass).Select((triple) => triple.Subject);
            foreach (var subject in aclSubjects)
            {
                var subjectNode = aclGraph.CreateBlankNode();
                foreach (var triple in graph.GetTriplesWithSubject(subject))
                {
                    aclGraph.Assert(new Triple(
                        subjectNode,
                        triple.Predicate,
                        triple.Object
                    ));
                }
            }

            yield return graph;
            yield return aclGraph;
        }

        yield return trellisGraph;
    }
}
