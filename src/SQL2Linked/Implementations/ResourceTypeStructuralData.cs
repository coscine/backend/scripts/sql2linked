﻿using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Model;
using SQL2Linked.Utils;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace SQL2Linked.Implementations;

/// <summary>
/// Class responsible for converting resource type data into linked data graphs.
/// It retrieves resource type information from the API and then transforms this data into a series of RDF graphs,
/// making use of predefined URIs and RDF constructs.
/// </summary>
public class ResourceTypeStructuralData : StructuralData<ResourceTypeInformationDto>
{
    public override async IAsyncEnumerable<ResourceTypeInformationDto> GetAll()
    {
        var resourceTypeApi = new ResourceTypeApi(_apiConfiguration);
        var resourceTypeInformationsResponse = await resourceTypeApi.GetAllResourceTypesInformationAsync();
        foreach (var item in resourceTypeInformationsResponse.Data)
        {
            yield return item; // Yield each item in the list to keep the IAsyncEnumerable logic
        }
    }

    public override async IAsyncEnumerable<IGraph> ConvertToLinkedDataAsync(IAsyncEnumerable<ResourceTypeInformationDto> entries)
    {
        await foreach (var entry in entries)
        {
            var resourceTypeGraphName = UriHelper.TryCombineUri(RdfUris.CoscineResourceTypes, entry.Id)
                ?? throw new Exception("Could not combine resource types prefix with resource type ID");
            var response = _adminApi.GetMetadataGraph(resourceTypeGraphName.AbsoluteUri, RdfFormat.TextTurtle);

            var graph = new Graph()
            {
                BaseUri = resourceTypeGraphName
            };

            graph.LoadFromString(response.Data.Content, new TurtleParser());

            // check if a triple with a dcat:DataService already exists in the resourcetype graph
            var getTriplesDcatDataService = graph.GetTriplesWithObject(RdfUris.DcatDataServiceClass);

            if (!getTriplesDcatDataService.Any())
            {
                AssertToGraphUriNode(graph, resourceTypeGraphName, RdfUris.A, RdfUris.DcatDataServiceClass);
                Console.WriteLine($"For resource type '{entry.SpecificType}' will migrate triple '{graph.BaseUri} {RdfUris.A} {RdfUris.DcatDataServiceClass}'. ");
            }
            else
            {
                Console.WriteLine($"For resource type '{entry.SpecificType}' will NOT migrate triple '{graph.BaseUri} {RdfUris.A} {RdfUris.DcatDataServiceClass}'. ");
            }

            // check if a triple with dcterms:title '{entry.DisplayName}' already exists in the role graph
            var getTriplesDctermsTitle = graph.GetTriplesWithPredicate(RdfUris.DcTermsTitle);

            if (!getTriplesDctermsTitle.Any())
            {
                AssertToGraphLiteralNode(graph, resourceTypeGraphName, RdfUris.DcTermsTitle, entry.SpecificType);
                Console.WriteLine($"For resource type '{entry.SpecificType}' will migrate triple '{graph.BaseUri} {RdfUris.DcTermsTitle} {entry.SpecificType}'. ");
            }
            else
            {
                Console.WriteLine($"For resource type '{entry.SpecificType}' will NOT migrate triple '{graph.BaseUri} {RdfUris.DcTermsTitle} {entry.SpecificType}'. ");
            }


            // check if a triple with dcterms:title '{entry.DisplayName}' already exists in the role graph
            var getTriplesDctermsAlternative = graph.GetTriplesWithPredicate(RdfUris.DcTermsAlternative);

            if (!getTriplesDctermsAlternative.Any())
            {
                AssertToGraphLiteralNode(graph, resourceTypeGraphName, RdfUris.DcTermsAlternative, entry.GeneralType);
                Console.WriteLine($"For resource type '{entry.SpecificType}' will migrate triple '{graph.BaseUri} {RdfUris.DcTermsAlternative} {entry.GeneralType}'. ");
            }
            else
            {
                Console.WriteLine($"For resource type '{entry.SpecificType}' will NOT migrate triple '{graph.BaseUri} {RdfUris.DcTermsAlternative} {entry.GeneralType}'. ");
            }

            if (!getTriplesDcatDataService.Any() || !getTriplesDctermsTitle.Any())
            {
                yield return graph;
            }
        }
    }
}