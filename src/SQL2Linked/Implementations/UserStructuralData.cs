﻿using Coscine.ApiClient;
using Coscine.ApiClient.Core.Model;
using SQL2Linked.Utils;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace SQL2Linked.Implementations;

/// <summary>
/// Class responsible for converting user data into linked data graphs.
/// It retrieves user information from the API and then transforms this data into a series of RDF graphs,
/// making use of predefined URIs and RDF constructs.
/// </summary>
public class UserStructuralData : StructuralData<UserDto>
{
    public override IAsyncEnumerable<UserDto> GetAll()
    {
        return PaginationHelper.GetAllAsync<UserDtoPagedResponse, UserDto>(
            (currentPage) => _adminApi.GetAllUsersAsync(tosAccepted: true, pageNumber: currentPage, pageSize: 50));
    }

    public override async IAsyncEnumerable<IGraph> ConvertToLinkedDataAsync(IAsyncEnumerable<UserDto> entries)
    {
        await foreach (var entry in entries)
        {
            var userGraphName = UriHelper.TryCombineUri(RdfUris.CoscineUsers, entry.Id)
                ?? throw new Exception("Could not combine users prefix with user ID");
            var response = _adminApi.GetMetadataGraph(userGraphName.AbsoluteUri, RdfFormat.TextTurtle);

            var graph = new Graph()
            {
                BaseUri = userGraphName
            };

            graph.LoadFromString(response.Data.Content, new TurtleParser());

            // check if a triple with a foaf:Person already exists in the user graph
            var getTriples = graph.GetTriplesWithObject(RdfUris.FoafPersonClass);
            // check if the current display name is already applied in the user graph
            var getTriplesName = graph.GetTriplesWithPredicate(RdfUris.FoafName);

            if (!getTriples.Any() || !getTriplesName.Any((triple) => (triple.Object as ILiteralNode)?.Value == entry.DisplayName))
            {
                AssertToGraphUriNode(graph, userGraphName, RdfUris.A, RdfUris.FoafPersonClass);
                Console.WriteLine($"For user '{entry.DisplayName}' will migrate triple '{userGraphName} {RdfUris.A} {RdfUris.FoafPersonClass}'. ");

                graph.Retract(getTriplesName);
                AssertToGraphLiteralNode(graph, userGraphName, RdfUris.FoafName, entry.DisplayName);

                Console.WriteLine($"Will migrate user '{entry.DisplayName}' with id '{entry.Id}'.");

                yield return graph;
            }
            else
            {
                Console.WriteLine($"Will NOT migrate user '{entry.DisplayName}' with id '{entry.Id}'.");
            }
        }
    }
}
