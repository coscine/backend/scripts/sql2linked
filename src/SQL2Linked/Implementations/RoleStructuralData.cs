﻿using Coscine.ApiClient;
using Coscine.ApiClient.Core.Api;
using Coscine.ApiClient.Core.Model;
using SQL2Linked.Utils;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace SQL2Linked.Implementations;

/// <summary>
/// Class responsible for converting role data into linked data graphs.
/// It retrieves role information from the API and then transforms this data into a series of RDF graphs,
/// making use of predefined URIs and RDF constructs.
/// </summary>
public class RoleStructuralData : StructuralData<RoleDto>
{
    public override IAsyncEnumerable<RoleDto> GetAll()
    {
        var roleApi = new RoleApi(_apiConfiguration);
        return PaginationHelper.GetAllAsync<RoleDtoPagedResponse, RoleDto>(
            (currentPage) => roleApi.GetRolesAsync(pageNumber: currentPage));
    }

    public override async IAsyncEnumerable<IGraph> ConvertToLinkedDataAsync(IAsyncEnumerable<RoleDto> entries)
    {
        await foreach (var entry in entries)
        {
            var roleGraphName = UriHelper.TryCombineUri(RdfUris.CoscineRoles, entry.Id)
                ?? throw new Exception("Could not combine role prefix with role ID");
            var response = await _adminApi.GetMetadataGraphAsync(roleGraphName.AbsoluteUri, RdfFormat.TextTurtle);

            var graph = new Graph()
            {
                BaseUri = roleGraphName
            };

            graph.LoadFromString(response.Data.Content, new TurtleParser());

            // check if a triple with a org:role already exists in the role graph
            var getTriplesOrgRole = graph.GetTriplesWithObject(RdfUris.OrgRoleClass);

            if (!getTriplesOrgRole.Any())
            {
                AssertToGraphUriNode(graph, roleGraphName, RdfUris.A, RdfUris.OrgRoleClass);
                Console.WriteLine($"For role '{entry.DisplayName}' will migrate triple '{graph.BaseUri} {RdfUris.A} {RdfUris.OrgRoleClass}'. ");
            }
            else
            {
                Console.WriteLine($"For role '{entry.DisplayName}' will NOT migrate triple '{graph.BaseUri} {RdfUris.A} {RdfUris.OrgRoleClass}'. ");
            }

            // check if a triple with dcterms:title '{entry.DisplayName}' already exists in the role graph
            var getTriplesDctermsTitle = graph.GetTriplesWithPredicate(RdfUris.DcTermsTitle);

            if (!getTriplesDctermsTitle.Any())
            {
                AssertToGraphLiteralNode(graph, roleGraphName, RdfUris.DcTermsTitle, entry.DisplayName);
                Console.WriteLine($"For role '{entry.DisplayName}' will migrate triple '{graph.BaseUri} {RdfUris.DcTermsTitle} {entry.DisplayName}'. ");
            }
            else
            {
                Console.WriteLine($"For role '{entry.DisplayName}' will NOT migrate triple '{graph.BaseUri} {RdfUris.DcTermsTitle} {entry.DisplayName}'. ");
            }
            if (!getTriplesOrgRole.Any() || !getTriplesDctermsTitle.Any())
            {
                yield return graph;
            }
        }
    }
}